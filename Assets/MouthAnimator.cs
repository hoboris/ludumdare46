﻿using System;
using System.Collections;
using System.Collections.Generic;
using Code.FoodTrack;
using UnityEngine;

public class MouthAnimator : MonoBehaviour
{
    private Mouth Mouth;
    private Animator Dino;

    private float JawOpening;
    public bool Opening;
    private static readonly int JawOpeningParam = Animator.StringToHash("JawOpening");

    private void Awake()
    {
        Dino = FindObjectOfType<Animator>();
        Mouth = GetComponent<Mouth>();
        JawOpening = 0;
    }

    private void Update()
    {
        var dif = Time.deltaTime * 5 * (Opening ? 1 : -1);
        JawOpening = Mathf.Clamp(JawOpening + dif, 0, 1);

        if (Mouth.Opened)
        {
            Opening = true;
        }
        else if (Opening && !Mouth.Opened && Mathf.Approximately(JawOpening, 1))
        {
            Opening = false;
        }

        Dino.SetFloat(JawOpeningParam, JawOpening);
    }
}