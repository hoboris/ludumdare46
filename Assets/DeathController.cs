﻿using System;
using System.Collections;
using System.Collections.Generic;
using Code;
using UnityEngine;

public class DeathController : MonoBehaviour
{

    private MovementController mv;
    private DinoWastedFeedback dwf;
    private CameraSwitch sw;
    private Animator Dino;
    private TimeScore score;
    private static readonly int Isdead = Animator.StringToHash("Isdead");

    private bool isDed;
    
    private void Awake()
    {

        var dinoHero = FindObjectOfType<HeroDino>();
        Dino = dinoHero.GetComponent<Animator>();
        sw = FindObjectOfType<CameraSwitch>();
        dwf = FindObjectOfType<DinoWastedFeedback>();
        mv = FindObjectOfType<MovementController>();
        score = FindObjectOfType<TimeScore>();
    }

    private void Update()
    {
        if (!isDed && Mathf.Approximately(mv.Speed, 0) )
        {
            isDed = true;
            sw.SwitchCamera("VC_Death");
            dwf.DinoWasted();
            Dino.SetBool(Isdead, true);
            score.isGameOver = true;
            foreach (var keyAction in FindObjectsOfType<KeyAction>())
            {
                keyAction.Disable();
            }
            
            foreach (var scheduler in FindObjectsOfType<ScheduledNeedStimuli>())
            {
                scheduler.gameObject.SetActive(false);
            }


            FindObjectOfType<Brain>().Die();
        }
    }
}
