﻿using System;
using Code;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class UIFeed : MonoBehaviour
{
    public GameObject FeedMessageTemplate;
    public Transform destination;
    public Color clearedColor, expiredColor;
    public float messageDuration;
    
    public void DisplayMessage(Need need)
    {
        var m = Instantiate(FeedMessageTemplate, transform.position, Quaternion.identity, transform);
        var state = "";
        var messageColor = Color.black;
        switch (need.currentState)
        {
            case NeedState.Pending:
                break;
            case NeedState.Clear:
                messageColor = clearedColor;
                state = "CLEARED!";
                break;
            case NeedState.Expired:
                messageColor = expiredColor;
                state = "EXPIRED!";
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        var message = need.needName + " " + state;

        m.GetComponentInChildren<UIWrapper>().SetWrapper(message, messageColor);
            
        Vector2 randPos;
        randPos.x = Random.Range(-1f, 1f);
        randPos.y = Random.Range(-1f, 1f);
        randPos.Normalize();

        m.transform.DOPunchScale(randPos * 0.25f, messageDuration);
        var shake = m.transform.DOShakeRotation(1);
        shake.SetLoops(-1);

        var move = m.transform.DOLocalMove(new Vector3(destination.position.x, destination.position.y, 0), messageDuration);
        move.SetEase(Ease.Linear);
        move.OnComplete(() => { Destroy(m); });
    }
}
