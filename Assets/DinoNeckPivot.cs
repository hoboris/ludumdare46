﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinoNeckPivot : MonoBehaviour
{
    public float ZUP;
    public float ZDOWN;

    private DinoNeck nck;

    private void Awake()
    {
        nck = GetComponent<DinoNeck>();
    }
    void Update()
    {
        var r = gameObject.transform.rotation.eulerAngles;
        r.z = ZUP + (ZDOWN - ZUP) * nck.position;
        gameObject.transform.rotation = Quaternion.Euler(r);
    }
}
