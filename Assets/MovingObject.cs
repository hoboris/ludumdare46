﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class MovingObject : MonoBehaviour
{
    public float Speed;
    
    private MovementController mvc;
    // Start is called before the first frame update
    void Start()
    {
        mvc = FindObjectOfType<MovementController>();
    }

    // Update is called once per frame
    void Update()
    {
        var o = gameObject;
        var pos = o.transform.position;
        pos.x += (Time.deltaTime * Speed)  - Time.deltaTime * mvc.Speed;
        o.transform.position = pos;

        if (pos.x >= 500 || pos.x <= -500)
        {
            Destroy(gameObject);
        }
    }
}
