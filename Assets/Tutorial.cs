﻿using System;
using System.Collections;
using System.Collections.Generic;
using Code;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    public static Tutorial Instance;
    
    public GameObject[] tutoHeart, tutoLungs, tutoMouth;
    
    private int tutoIndex = 0;
    
    private void Start()
    {
        LungsTuto(false);
        MouthTuto(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.H) && tutoIndex == 0)
        {
            StartGame();
            LungsTuto(true);
            tutoIndex++;

        }
        if (Input.GetKeyDown(KeyCode.L) && tutoIndex == 1)
        {
            LungsTuto(false);
            MouthTuto(true);
            tutoIndex++;
        }
        if (Input.GetKeyDown(KeyCode.M) && tutoIndex == 2)
        {
            MouthTuto(false);
            tutoIndex++;
        }
    }

    private void StartGame()
    {
        foreach (var element in tutoHeart)
        {
            element.SetActive(false);
        }
    }

    private void LungsTuto(bool openClose)
    {
        foreach (var element in tutoLungs)
        {
            element.SetActive(openClose);
        }
    }

    private void MouthTuto(bool openClose)
    {
        foreach (var element in tutoMouth)
        {
            element.SetActive(openClose);
        }
    }
}
