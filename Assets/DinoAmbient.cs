﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class DinoAmbient : MonoBehaviour
{
    private AudioSource audioSource;
    
    public AudioClip[] clips;

    public float minInterval, maxInterval;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        StartCoroutine(PlayRandomAmbientSound(0));
    }

    public IEnumerator PlayRandomAmbientSound(float interval)
    {
        var randomClip = clips[Random.Range(0, clips.Length)];

        var nextInterval  = Random.Range(minInterval, maxInterval);
        var randPitch = Random.Range(0.3f, 1f);
        
        yield return new WaitForSeconds(interval);

        audioSource.pitch = randPitch;
        audioSource.clip = randomClip;
        audioSource.Play();

        yield return StartCoroutine(PlayRandomAmbientSound(nextInterval));
    }
}
