﻿using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DinoWastedFeedback : MonoBehaviour
{
    public TextMeshProUGUI text;

    public float duration;
    public float punchIntensity;

    private void Start()
    {
        text.DOFade(0, 0);
    }

    public void DinoWasted()
    {
        
        text.DOFade(1, duration);
        var dopunch = text.transform.DOPunchScale(Vector3.one * punchIntensity, duration);

        StartCoroutine(lacoroutinedegueu());
    }

    IEnumerator lacoroutinedegueu()
    {
        yield return new WaitForSeconds(5);
        
        SceneManager.LoadScene(0);
    }

}
