﻿using DG.Tweening;
using TMPro;
using UnityEngine;

public class TitleEffects : MonoBehaviour
{
    public TextMeshProUGUI title;
    public TextMeshProUGUI subTitle;
    public float titleAppearingDuration = 5f;

    private void Start()
    {
        subTitle.DOFade(0,0);
        title.transform.DOSpiral(titleAppearingDuration, null, SpiralMode.Expand);
        var doText = title.DOText(title.text, titleAppearingDuration);
        doText.OnComplete(() => subTitle.DOFade(1, titleAppearingDuration + 5f));
        var dopunch = title.transform.DOPunchRotation(new Vector3(0,0,100), titleAppearingDuration, 12, 1);
    }
}
