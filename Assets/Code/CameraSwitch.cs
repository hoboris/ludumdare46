﻿using System;
using Cinemachine;
using UnityEngine;

public class CameraSwitch : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera[] cameras;

    [SerializeField] private CinemachineVirtualCamera onAir;
    private int cameraIndex = 0;

    public GameObject EyesVignette;

    private bool isNeckDown = false;

    private string lastCam;
    
    private void Awake()
    {
        cameras = GetComponentsInChildren<CinemachineVirtualCamera>();
    }

    private void Start()
    {
        foreach (var virtualCamera in cameras)
        {
            virtualCamera.gameObject.SetActive(false);
        }
        SwitchCamera("VC_Body");
    }

    // private void OnGUI()
    // {
    //     var yOffset = 20f;
    //
    //     for (int i = 0; i < cameras.Length; i++)
    //     {
    //         if(GUI.Button(new Rect(10,10 + i*yOffset,100,20), cameras[i].gameObject.name + ""))
    //         {
    //             SwitchCamera(cameras[i].gameObject.name);
    //         }
    //     }
    // }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SwitchCamera();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            SwitchCamera("VC_Eyes");
            EyesVignette.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.E))
        {
            if (isNeckDown)
            {
                SwitchCamera("VC_Neck");
            }
            else if(!isNeckDown)
            {
                SwitchCamera("VC_Body");
            }            
            EyesVignette.SetActive(false);
        }
        
        
        if (Input.GetKeyUp(KeyCode.N))
        {
            if (isNeckDown)
            {
                SwitchCamera("VC_Body");
                isNeckDown = false;
            }
            else if(!isNeckDown)
            {
                SwitchCamera("VC_Neck");
                isNeckDown = true;
            }
            EyesVignette.SetActive(false);
        }
    }

    public void SwitchCamera(string cameraName)
    {
        
        var camToSwitchTo = GetCamera(cameraName);
        
        if(onAir != null) onAir.gameObject.SetActive(false);
        onAir = camToSwitchTo;
        onAir.gameObject.SetActive(true);
    }
    
    public void SwitchCamera()
    {
        cameraIndex++;

        if (cameraIndex >= cameras.Length)
        {
            cameraIndex = 0;
        }
        
        SwitchCamera(cameras[cameraIndex].gameObject.name);
    }

    CinemachineVirtualCamera GetCamera(string cameraName)
    {
        for (int i = 0; i < cameras.Length; i++)
        {
            if (cameras[i].name == cameraName)
            {
                return cameras[i];
            }
        }
        //Debug.LogError("Cannot find camera with this name brodude wtf get your shit together");
        return null;
    }
}
