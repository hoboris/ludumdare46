using UnityEngine;

namespace Code.FoodTrack
{
    public interface FoodTrack
    {
        bool Pass(Food food);
    }
}