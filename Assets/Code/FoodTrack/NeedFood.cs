﻿using System.Collections;
using System.Collections.Generic;
using Code;
using UnityEngine;

public class NeedFood : TimeNeed
{

    public override bool ClearedBy(Action a)
    {
        return a is Eat;
    }

    public override void OnExpiration()
    {
    }
    
}
