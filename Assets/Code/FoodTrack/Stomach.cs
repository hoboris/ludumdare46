﻿using System;
using Code;
using Code.FoodTrack;
using UnityEngine;

[RequireComponent(typeof(KeyAction))]
public class Stomach : ActionReleaser, FoodTrack
{
    private KeyAction KeyAction;

    private Bowel bowels;
    
    private Food ToProcess;


    private void Awake()
    {
        KeyAction = GetComponent<KeyAction>();
        bowels = FindObjectOfType<Bowel>();
        brain = FindObjectOfType<Brain>();
    }

    private void Start()
    {
        KeyAction.Disable();
    }

    public bool Pass(Food food)
    {
        if (ToProcess != null) return false;

        ToProcess = food;
        food.gameObject.transform.SetParent(transform);
        food.gameObject.transform.position = transform.position;
        KeyAction.Enable();
        return true;
    }

    private void Update()
    {
        if (ToProcess != null && KeyAction.Pressed())
        {
            if (bowels.Pass(ToProcess))
            {
                ToProcess = null;
                SendAction();
                KeyAction.Disable();
            }
        }
    }
    
}
