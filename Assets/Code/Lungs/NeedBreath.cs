namespace Code.Lungs
{
    public class NeedBreath : TimeNeed
    {
        public override bool ClearedBy(Action a)
        {
            return a is Breath;
        }

        public override void OnExpiration()
        {
        }
    }
}