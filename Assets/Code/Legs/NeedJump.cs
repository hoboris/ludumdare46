using UnityEngine;

namespace Code
{
    public class NeedJump : DistanceNeed
    {
        public override bool ClearedBy(Action a)
        {
            return a is Jump;
        }

        public override void OnExpiration()
        {
        }
    }
}