﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace Code
{
    public class NeedEvent : UnityEvent<Need>
    {
    }

    public class Brain : MonoBehaviour
    {
        public NeedEvent OnNeedSent = new NeedEvent();

        private List<Need> Needs;

        private MovementController mvc;

        private void Awake()
        {
            mvc = FindObjectOfType<MovementController>();
            Needs = new List<Need>();
        }

        public void SendNeed(Need need)
        {
            Needs.Add(need);

            OnNeedSent.Invoke(need);
        }

        public void SendAction(Action action)
        {
            var cleared = Needs.Where(n => n.ClearedBy(action)).ToList();
            foreach (Need need in cleared)
            {
                need.SetState(NeedState.Clear);
                RemoveNeed(need);
            }

            if (cleared.Count > 0)
            {
                mvc.SpeedUp(1);
            }
            else
            {
                mvc.SpeedDown(2);
            }

            Destroy(action.gameObject);
        }

        private void RemoveNeed(Need need)
        {
            Needs.Remove(need);
            need.OnDeath();
            Destroy(need.gameObject);
        }

        // Update is called once per frame
        void Update()
        {
            var expired = Needs.Where(n => n.Expired()).ToList();
            foreach (var need in expired)
            {
                mvc.SpeedDown(3);
                need.SetState(NeedState.Expired);
                RemoveNeed(need);
                need.OnExpiration();
            }
        }

        public void Die()
        {
            foreach (Need need in Needs)
            {
                need.SetState(NeedState.Expired);
                RemoveNeed(need);
            }
        }
    }
}