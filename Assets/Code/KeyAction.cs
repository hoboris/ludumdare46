using UnityEngine;
using UnityEngine.Events;

namespace Code
{
    public class KeyActionEvent:  UnityEvent<KeyAction> {}
    
    public class KeyAction : MonoBehaviour
    {
        public KeyCode KeyCode;
        public AudioClip clip;
        
        [HideInInspector]
        public bool Enabled;
        
        [HideInInspector]
        public UnityEvent OnKeyPressed = new UnityEvent();
        public KeyActionEvent StateChange = new KeyActionEvent();

        private void Awake()
        {
            Enabled = true;
        }

        public void Enable()
        {
            if (Enabled) return;

            
            Enabled = true;
            StateChange.Invoke(this);
        }
        
        public void Disable()
        {
            if (!Enabled) return;
            Enabled = false;
            StateChange.Invoke(this);
        }

        public bool Pressed()
        {
            if (!Enabled) return false;
            
            var pressed = Input.GetKeyDown(KeyCode);
            if (pressed)
            {
                OnKeyPressed.Invoke();
                SoundManager.Instance.PlayOrganSFX(clip);
            }
            return pressed;
        }
    }
}