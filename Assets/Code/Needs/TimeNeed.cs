using System;
using UnityEngine;

namespace Code
{
    public abstract class TimeNeed : Need
    {
        public float Expiration;
        
        private float dt;

        private void Start()
        {
            dt = 0;
        }

        private void Update()
        {
            dt += Time.deltaTime;
        }

        public override bool Expired()
        {
            return dt >= Expiration;
        }

        public override float PercentToExpiration()
        {
            return dt / Expiration;
        }
    }
}