using System;
using UnityEngine;

namespace Code
{
    public class DistanceNeedStimuli : NeedStimuli
    {
        public float Distance;
        public Transform DinoTransform;
        private bool Triggered;
        
        private void Update()
        {
            if (!Triggered && gameObject.transform.position.x - DinoTransform.position.x < Distance)
            {
                SendNeed();
                Triggered = true;
            }
        }
    }
}