using System;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

namespace Code
{
    public abstract class DistanceNeed : Need
    {
        public Transform DinoTransform;
        public float ExpirationDistance;
        
        private float originX;

        private void Awake()
        {
            originX = gameObject.transform.position.x;
        }

        public override bool Expired()
        {
            return gameObject.transform.position.x - DinoTransform.position.x < ExpirationDistance;
        }
        public override float PercentToExpiration()
        {
            return Math.Abs(originX - gameObject.transform.position.x) /
                   Math.Abs(originX - DinoTransform.position.x - ExpirationDistance); ;
        }
    }
}