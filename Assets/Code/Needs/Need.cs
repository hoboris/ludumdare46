using System;
using UnityEngine;

namespace Code
{
    public enum NeedState
    {
        Pending,
        Clear,
        Expired
    }
    
    public abstract class Need : MonoBehaviour
    {
        public Sprite sprite;
        public string needName;
        
        public NeedEvent OnDeathEvent = new NeedEvent();
        
        public NeedState currentState;

        private void Start()
        {
            currentState = NeedState.Pending;
        }

        public void SetState(NeedState state)
        {
            currentState = state;
        }

        public void OnDeath()
        {
            OnDeathEvent.Invoke(this);
        }
        
        public abstract bool ClearedBy(Action a);

        public abstract void OnExpiration();

        public abstract bool Expired();
        
        public abstract float PercentToExpiration();
        
    }
}