using UnityEngine;

namespace Code
{
    public class ScheduledNeedStimuli : NeedStimuli
    {
        public float Period;

        private float dt;
        private void Update()
        {
            dt += Time.deltaTime;
            if (dt >= Period)
            {
                SendNeed();
                dt -= Period;
            }
        }
    }
}