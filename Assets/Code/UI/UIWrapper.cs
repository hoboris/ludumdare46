﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class UIWrapper : MonoBehaviour
{
    public TextMeshProUGUI inputText;
    [SerializeField] private Image icon;
    
    public void SetWrapper(string textToSet, Color color)
    {
        SetWrapper(textToSet);
        inputText.color = color;
    }
    
    public void SetWrapper(string textToSet)
    {
        if (inputText == null) GetComponent<TextMeshProUGUI>();

        inputText.text = textToSet;
    }

    public void SetWrapper(Sprite spr)
    {
        icon.sprite = spr;
    }
}
