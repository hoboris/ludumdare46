using System;
using UnityEngine;
using UnityEngine.Events;

namespace Code
{
    [RequireComponent(typeof(KeyAction))]
    public class KeyPressActionReleaser : ActionReleaser
    {
        private KeyAction KeyAction;

        private void Awake()
        {
            KeyAction = GetComponent<KeyAction>();
        }

        private void Update()
        {
            if (KeyAction.Pressed())
            {
                SendAction();
            }
        }
    }
}