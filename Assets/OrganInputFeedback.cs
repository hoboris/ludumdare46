﻿using System;
using Code;
using DG.Tweening;
using UnityEngine;

public class OrganInputFeedback : MonoBehaviour
{
    private UIWrapper wrapper;
    private Animator bgAnim;

    [SerializeField] private float punchIntensity;
    [SerializeField] private float punchDuration;

    private Vector3 baseScale;
    
    private void Awake()
    {
        wrapper = GetComponent<UIWrapper>();
        bgAnim = GetComponentInChildren<Animator>();
        bgAnim.gameObject.SetActive(false);
    }

    private void Start()
    {
        baseScale = transform.localScale;
    }

    public void InputPressedFeedback()
    {
        var t = wrapper.inputText;
        
        bgAnim.gameObject.SetActive(true);
        bgAnim.SetTrigger("PlayBG");

        var punch = transform.DOPunchScale(Vector3.one * punchIntensity, punchDuration, 3, .5f);
        punch.OnComplete(() =>
        {
            transform.localScale = baseScale;
            bgAnim.gameObject.SetActive(false);
        });
    }
}
