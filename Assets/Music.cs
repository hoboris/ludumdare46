﻿
using UnityEngine;

public class Music : MonoBehaviour
{
    private AudioSource audioSource;
    private AudioLowPassFilter lowPassFilter;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        lowPassFilter = GetComponent<AudioLowPassFilter>();
    }

    public void ApplyLowPass(float freq)
    {
        lowPassFilter.cutoffFrequency = freq;
    }
}
