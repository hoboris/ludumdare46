﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class HeroDino : MonoBehaviour
{

    private Animator Dino;
    private MovementController m;
    private static readonly int SpeedParam = Animator.StringToHash("Speed");
    private void Awake()
    {
        m = FindObjectOfType<MovementController>();
        Dino = GetComponent<Animator>();
    }

    private void Update()
    {
        Dino.SetFloat(SpeedParam, m.NormalizedSpeed);
    }
    
}
