﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class BrainNeedBubbleFeedback : MonoBehaviour
{
    public Image bubble;
    public GameObject Icon;

    public Animator anim;
    
    private void Awake()
    {
        anim = GetComponentInChildren<Animator>();
        bubble = GetComponent<Image>();
    }

    public void DestroyBubble()
    {
        bubble.color = Color.clear;
        anim.GetComponent<SpriteRenderer>().color = Color.white;
        Icon.SetActive(false);
        anim.Play("brainBubble_out");
    }

    public void OnAnimEnded()
    {
        Destroy(transform.parent.gameObject);
    }
}
